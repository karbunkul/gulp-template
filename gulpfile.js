/**
 * Created by karbunkul on 06/01/15.
 */
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var watch = require('gulp-watch');
var livereload = require('gulp-livereload');
var server = require('gulp-server-livereload');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');

var srcSCSS = 'assets/scss';
var outCSS = 'assets/css';

/* Задачи по умолчанию при вызове Gulp */
gulp.task('default', ['watch', 'sass']);

// Отслеживание изменений в файлах
gulp.task('watch', function () {

    livereload.listen();
    gulp.watch(srcSCSS + '/*.scss', ['sass']);
    gulp.watch('*.html', ['html']);

});

/* Livereload для HTML файлов */
gulp.task('html', function () {

    gulp.src('./').pipe(livereload());

});

/* Генерация css из scss */
gulp.task('sass', function () {

    gulp.src(srcSCSS + '/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write())

        // Autoprefix
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(autoprefixer('last 2 versions', 'ie 8', 'ie 6'))

        .pipe(sourcemaps.write('./maps'))

        .pipe(gulp.dest(outCSS))
        .pipe(livereload());

});

/* Запуск локального сервера */
gulp.task('webserver', ['default'], function () {

    gulp.src('./')
        .pipe(server({
            livereload: true,
            directoryListing: false,
            defaultFile: 'index.html',
            open: true
        }));

});